import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestBing {

  @Test
  public void TestBing{
  WebDriver driver = new FirefoxDriver();
  driver.get("https://www.bing.com/");
  System.out.println("Page Title is " + driver.getTitle());
  Assert.assertEquals("Bing", driver.getTitle());

  WebElement findLine = driver.findElement(By.className("sbox"));
  findLine.sendKeys("Hello world");

  WebElement searchButton = driver.findElement(By.className("sbox"));
  searchButton.click();

  findLine = driver.findElement(By.className("sbox"));
  Assert.assertEquals("Hello world", findLine.getAttribute("value"));
  driver.quit();

}
}
